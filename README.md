# A Drupal install profile for Twelve Step Intergroups

This will be a Drupal 8.x install profile for Twelve Step (AA, Alanon, NA, CA, etc...) Intergroups (Intergroups are also known as Central Offices or Central Service Offices). Collaboration or use of this project does not imply membership in AA or in any other 12 step fellowship. Should you be in a 12 step fellowship and have concerns regarding anonymity, we suggest you open an anonymous drupal.org account before commenting or contributing here. We will always maintain an individual's anonymity.

This project is new and a also work-in-progress. Should you wish to join the group developing this project, please contact me. We are having weekly meetings on Friday afternoons to collaborate on the tasks and stay on target for weekly progress.

A sample site using the profile is available at http://twelvestepintergroup.org/

## Drupal 8

This project implements a Drupal 8.x install profile (also known as a distribution).

The intent is to have a complete package that can be downloaded from drupal.org or installed on hosting platforms
such as Acquia or Pantheon.

## Steps used to create this profile and a new site

Download from Drupal.org project page (https://www.drupal.org/project/twelvestepintergroup)

or

Install from composer
```
$ curl -sS https://getcomposer.org/installer | php
$ composer create-project "drupal-composer/drupal-project:~8.1" mycityaa --stability dev
$ cd mycityaa
$ composer config repositories.drupal composer https://packages.drupal.org/8
$ composer require drupal/twelvestepintergroup
```
Then install a new site:
```
$ drush -y si twelvestepintergroup --site-name="My Cities Alcoholics Anonymous Central Office"
```

## Export and Import content

The content specific to this profile should be migrated.

Create the migration templates from the examples in the twelvestepmigrate/migration_templates directory,
enable the twelvestepmigrate module, then import your the content based on the migration_tag.

```
  drush en twelvestepmigrate
```

Or in your custom modules hook_install().

```
  \Drupal::service('module_installer')->install(['twelvestepmigrate']);
  twelvestepmigrate_import('example-csv');
```
